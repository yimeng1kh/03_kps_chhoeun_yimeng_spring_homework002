package com.example.homework002.service.ServiceImp;

import com.example.homework002.model.entity.Customer;
import com.example.homework002.model.request.CustomerRequest;
import com.example.homework002.repository.CustomerRepository;
import com.example.homework002.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CustomerServiceImp implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomers() {
       return customerRepository.findAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public Customer insertCustomer(CustomerRequest customerRequest) {
        return customerRepository.insertCustomer(customerRequest);
    }


    @Override
    public Customer updateCustomer(CustomerRequest customerRequest, Integer id) {
        return customerRepository.updateCustomer(id);
    }

    @Override
    public Customer deleteCustomer( Integer id) {
        return customerRepository.deleteCustomer(id);
    }



}
