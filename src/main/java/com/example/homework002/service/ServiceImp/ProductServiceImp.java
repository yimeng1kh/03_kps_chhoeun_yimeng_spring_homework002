package com.example.homework002.service.ServiceImp;

import com.example.homework002.model.entity.Product;
import com.example.homework002.model.request.ProductRequest;
import com.example.homework002.repository.ProductRepository;
import com.example.homework002.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
    @Override
    public List<Product> getAllProduct() {
        return productRepository.getAllProduct();
    }

    @Override
    public Product getProductById(Integer id) {
        return productRepository.getProductById(id);
    }

    @Override
    public Product insertProduct(ProductRequest productRequest) {
        return productRepository.insertProduct(productRequest);
    }

    @Override
    public Product updateProduct(ProductRequest productRequest, Integer id) {
        return productRepository.updateProduct(id);
    }

    @Override
    public Product deleteProduct(Integer id) {
        return productRepository.deleteProduct(id);
    }


}
