package com.example.homework002.service;


import com.example.homework002.model.entity.Customer;
import com.example.homework002.model.request.CustomerRequest;

import java.util.List;

//@Service
public interface CustomerService {
    List<Customer> getAllCustomers();

    Customer getCustomerById(Integer id);


    Customer insertCustomer(CustomerRequest customerRequest);

    Customer updateCustomer(CustomerRequest customerRequest, Integer id);

    Customer deleteCustomer(Integer id);
}
