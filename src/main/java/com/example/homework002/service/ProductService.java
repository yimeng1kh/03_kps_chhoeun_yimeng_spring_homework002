package com.example.homework002.service;

import com.example.homework002.model.entity.Product;
import com.example.homework002.model.request.ProductRequest;

import java.util.List;

public interface ProductService  {
    List<Product> getAllProduct();

    Product getProductById(Integer id);

    Product insertProduct(ProductRequest productRequest);

    Product updateProduct(ProductRequest productRequest, Integer id);

    Product deleteProduct(Integer id);


}
