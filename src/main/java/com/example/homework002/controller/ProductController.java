package com.example.homework002.controller;

import com.example.homework002.model.entity.Customer;
import com.example.homework002.model.entity.Product;
import com.example.homework002.model.request.CustomerRequest;
import com.example.homework002.model.request.ProductRequest;
import com.example.homework002.model.response.CustomerResponse;
import com.example.homework002.model.response.ProductResponse;
import com.example.homework002.service.CustomerService;
import com.example.homework002.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/api/v1/products")

public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService){
        this.productService= productService;
    }

    @GetMapping("/all")
    @Operation(summary="Get all products")
    public ResponseEntity<List<Product>> getAllProduct(){
        return ResponseEntity.ok(productService.getAllProduct());
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getProductById(@PathVariable Integer id){
        Product product = productService.getProductById(id);
        ProductResponse<Product> response= new ProductResponse<>(
                product,
                "Successfully get Product ID"

        );
        return ResponseEntity.ok(response);
    }

    @PostMapping("/add-new product")
    public ResponseEntity<?> insertProduct(@RequestBody ProductRequest productRequest){
        ProductResponse<Product> response= new ProductResponse<Product>(
                productService.insertProduct(productRequest),
                "Added Successfully"
        );
        return ResponseEntity.ok(response);
    }

    @PutMapping("/update-by-{id}")
    public ResponseEntity<?> updateProduct(@PathVariable Integer id, @RequestBody ProductRequest productRequest){
        ProductResponse<Product> response= new ProductResponse<>(
                productService.updateProduct(productRequest,id),
                "Update Successfully"
        );
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/delete-by-{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable Integer id){
        ProductResponse<Product> response= new ProductResponse<Product>(
                productService.deleteProduct( id),
                "Delete Successfully"
        );
        return ResponseEntity.ok(response);
    }
}
