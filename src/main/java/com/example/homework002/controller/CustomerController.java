package com.example.homework002.controller;


import com.example.homework002.model.entity.Customer;
import com.example.homework002.model.request.CustomerRequest;
import com.example.homework002.model.response.CustomerResponse;
import com.example.homework002.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/all")
    @Operation(summary="Get all customers")
    public ResponseEntity<List<Customer>> getAllCustomer(){
       return ResponseEntity.ok(customerService.getAllCustomers());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable Integer id){
       Customer customer = customerService.getCustomerById(id);
        CustomerResponse<Customer>response= new CustomerResponse<>(
                customer,
                "Successfully get Customer ID"

        );
       return ResponseEntity.ok(response);
    }

    @PostMapping("/add-new customer")
   public ResponseEntity<?> insertCustomer(@RequestBody CustomerRequest customerRequest){
        CustomerResponse<Customer> response= new CustomerResponse<Customer>(
            customerService.insertCustomer(customerRequest),
                "Added Successfully"
        );
    return ResponseEntity.ok(response);

    }

    @PutMapping("/update-by-{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable Integer id, @RequestBody CustomerRequest customerRequest){
        CustomerResponse<Customer> response= new CustomerResponse<Customer>(
                customerService.updateCustomer(customerRequest,id),
                "Update Successfully"
        );
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/delete-by-{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Integer id){
        CustomerResponse<Customer> response= new CustomerResponse<Customer>(
                customerService.deleteCustomer( id),
                "Delete Successfully"
        );
        return ResponseEntity.ok(response);
    }

}
