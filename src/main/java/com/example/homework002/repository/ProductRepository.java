package com.example.homework002.repository;

import com.example.homework002.model.entity.Customer;
import com.example.homework002.model.entity.Product;
import com.example.homework002.model.request.CustomerRequest;
import com.example.homework002.model.request.ProductRequest;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;

import java.util.List;
@Mapper
public interface ProductRepository {
    @Select("SELECT * FROM product_tb")
    @Result(property = "id",column = "product_id")
    @Result(property = "name", column = "product_name")
    @Result(property = "price",column = "product_price")
    List<Product> getAllProduct();

    @Select("SELECT * FROM product_tb WHERE product_id = #{id}")
    @Result(property = "id",column = "product_id")
    @Result(property = "name", column = "product_name")
    @Result(property = "price",column = "product_price")
    Product getProductById(Integer id);


    @Select("""
            insert into product_tb ( product_name, product_price) 
            values (#{product.name}, #{product.price}) returning *
            """)
    @Result(property = "id",column = "product_id")
    @Result(property = "name", column = "product_name")
    @Result(property = "price",column = "product_price")
    Product insertProduct(@Param("product") ProductRequest productRequest);

    @Select("""
            update product_tb set product_name=#{product.name} , product_price=#{product.price} where product_id=#{id} returning *
            """)
    @Result(property = "id",column = "product_id")
    @Result(property = "name", column = "product_name")
    @Result(property = "price",column = "product_price")
    Product updateProduct(Integer id);

    @Select("DELETE from product_tb where product_id= #{id}")
    Product deleteProduct( Integer id);
}
