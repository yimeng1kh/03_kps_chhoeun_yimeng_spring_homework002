package com.example.homework002.repository;


import com.example.homework002.model.entity.Customer;
import com.example.homework002.model.request.CustomerRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Select("SELECT * FROM customer_tb")
    @Result(property = "id",column = "customer_id")
    @Result(property = "name", column = "customer_name")
    @Result(property = "address",column = "customer_address")
    @Result(property = "phone", column = "customer_phone")
    List<Customer> findAllCustomer();


    @Select("SELECT * FROM customer_tb WHERE customer_id = #{id}")
    @Result(property = "id",column = "customer_id")
    @Result(property = "name", column = "customer_name")
    @Result(property = "address",column = "customer_address")
    @Result(property = "phone", column = "customer_phone")
    Customer getCustomerById(Integer id);

    @Select("""
            insert into customer_tb ( customer_name, customer_address, customer_phone) 
            values (#{customer.name}, #{customer.address},#{customer.phone}) returning *
            """)
    @Result(property = "id",column = "customer_id")
    @Result(property = "name", column = "customer_name")
    @Result(property = "address",column = "customer_address")
    @Result(property = "phone", column = "customer_phone")
    Customer insertCustomer(@Param("customer") CustomerRequest customerRequest);

    @Select("""
            update customer_tb set customer_name=#{customer.name} , customer_address=#{customer.address},customer_phone=#{customer.phone} where customer_id=#{id} returning *
            """)
    @Result(property = "id",column = "customer_id")
    @Result(property = "name", column = "customer_name")
    @Result(property = "address",column = "customer_address")
    @Result(property = "phone", column = "customer_phone")
    Customer updateCustomer(Integer id);

    @Select("DELETE from customer_tb where customer_id= #{id}")
    Customer deleteCustomer( Integer id);
}
