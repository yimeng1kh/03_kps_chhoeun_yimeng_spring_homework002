package com.example.homework002.model.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerResponse<T> {
    private T load;
    private String message;
    //private boolean success= false;


}
