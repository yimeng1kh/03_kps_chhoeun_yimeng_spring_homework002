create table product_tb
(
    product_id    serial primary key,
    product_name  varchar(100),
    product_price numeric(7, 2)
);

create table customer_tb
(
    customer_id      serial primary key,
    customer_name    varchar(100),
    customer_address varchar(100),
    customer_phone   int4
);

create table invoice_tb
(
    invoice_id   serial primary key,
    invoice_date timestamp default now(),
    customer_id  int not null,

    foreign key (customer_id) references customer_tb (customer_id) on delete cascade on update cascade
);
create table invoice_detail_tb(
                         id serial primary key ,
                         invoice_id int ,
                         product_id int ,

                         FOREIGN KEY (invoice_id) references invoice_tb(invoice_id) on delete cascade on update cascade,
                         FOREIGN KEY (product_id) References product_tb (product_id) on delete cascade on update cascade
);